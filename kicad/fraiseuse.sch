EESchema Schematic File Version 4
LIBS:fraiseuse-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_Module:Arduino_UNO_R3 A?
U 1 1 5BBF57F6
P 3150 1800
F 0 "A?" H 3150 2978 50  0000 C CNN
F 1 "Arduino_UNO_R3" H 3150 2887 50  0000 C CNN
F 2 "Module:Arduino_UNO_R3" H 3300 750 50  0001 L CNN
F 3 "https://www.arduino.cc/en/Main/arduinoBoardUno" H 2950 2850 50  0001 C CNN
	1    3150 1800
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR0101
U 1 1 5BBF5B18
P 950 4250
F 0 "#PWR0101" H 950 4100 50  0001 C CNN
F 1 "+12V" H 965 4423 50  0000 C CNN
F 2 "" H 950 4250 50  0001 C CNN
F 3 "" H 950 4250 50  0001 C CNN
	1    950  4250
	1    0    0    -1  
$EndComp
$Comp
L Motor:Stepper_Motor_bipolar M?
U 1 1 5BBF5E95
P 10450 1100
F 0 "M?" H 10638 1223 50  0000 L CNN
F 1 "Stepper_Motor_bipolar" H 10638 1132 50  0000 L CNN
F 2 "" H 10460 1090 50  0001 C CNN
F 3 "http://www.infineon.com/dgdl/Application-Note-TLE8110EE_driving_UniPolarStepperMotor_V1.1.pdf?fileId=db3a30431be39b97011be5d0aa0a00b0" H 10460 1090 50  0001 C CNN
	1    10450 1100
	1    0    0    -1  
$EndComp
$Comp
L Motor:Stepper_Motor_bipolar M?
U 1 1 5BBF5F54
P 10700 1800
F 0 "M?" H 10888 1923 50  0000 L CNN
F 1 "Stepper_Motor_bipolar" H 10888 1832 50  0000 L CNN
F 2 "" H 10710 1790 50  0001 C CNN
F 3 "http://www.infineon.com/dgdl/Application-Note-TLE8110EE_driving_UniPolarStepperMotor_V1.1.pdf?fileId=db3a30431be39b97011be5d0aa0a00b0" H 10710 1790 50  0001 C CNN
	1    10700 1800
	1    0    0    -1  
$EndComp
$Comp
L Motor:Stepper_Motor_bipolar M?
U 1 1 5BBF5FFA
P 10700 2900
F 0 "M?" H 10888 3023 50  0000 L CNN
F 1 "Stepper_Motor_bipolar" H 10888 2932 50  0000 L CNN
F 2 "" H 10710 2890 50  0001 C CNN
F 3 "http://www.infineon.com/dgdl/Application-Note-TLE8110EE_driving_UniPolarStepperMotor_V1.1.pdf?fileId=db3a30431be39b97011be5d0aa0a00b0" H 10710 2890 50  0001 C CNN
	1    10700 2900
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x25_Female J?
U 1 1 5BC06BBA
P 8050 2150
F 0 "J?" H 8077 2176 50  0000 L CNN
F 1 "Conn_01x25_Female" H 8077 2085 50  0000 L CNN
F 2 "" H 8050 2150 50  0001 C CNN
F 3 "~" H 8050 2150 50  0001 C CNN
	1    8050 2150
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x25_Male J?
U 1 1 5BC06D7B
P 9250 2150
F 0 "J?" H 9223 2173 50  0000 R CNN
F 1 "Conn_01x25_Male" H 9223 2082 50  0000 R CNN
F 2 "" H 9250 2150 50  0001 C CNN
F 3 "~" H 9250 2150 50  0001 C CNN
	1    9250 2150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5BC0801C
P 950 4700
F 0 "#PWR0102" H 950 4450 50  0001 C CNN
F 1 "GND" H 955 4527 50  0000 C CNN
F 2 "" H 950 4700 50  0001 C CNN
F 3 "" H 950 4700 50  0001 C CNN
	1    950  4700
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push_SPDT SW?
U 1 1 5BC36025
P 10100 4300
F 0 "SW?" H 10100 4585 50  0000 C CNN
F 1 "SW_Push_SPDT" H 10100 4494 50  0000 C CNN
F 2 "" H 10100 4300 50  0001 C CNN
F 3 "" H 10100 4300 50  0001 C CNN
	1    10100 4300
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push_SPDT endstopX
U 1 1 5BC360E9
P 10100 3700
F 0 "endstopX" H 10100 3985 50  0000 C CNN
F 1 "SW_Push_SPDT" H 10100 3894 50  0000 C CNN
F 2 "" H 10100 3700 50  0001 C CNN
F 3 "" H 10100 3700 50  0001 C CNN
	1    10100 3700
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push_SPDT SW?
U 1 1 5BC3617E
P 10100 4850
F 0 "SW?" H 10100 5135 50  0000 C CNN
F 1 "SW_Push_SPDT" H 10100 5044 50  0000 C CNN
F 2 "" H 10100 4850 50  0001 C CNN
F 3 "" H 10100 4850 50  0001 C CNN
	1    10100 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 2900 2800 4700
Connection ~ 2800 4700
Wire Wire Line
	5700 800  5700 4650
$Comp
L DriverStepperMotorTB66:driverStepperMotorTB66 U?
U 1 1 5BC37782
P 2550 5500
F 0 "U?" V 1700 6350 50  0000 R CNN
F 1 "driverStepperMotorTB66 axe X" V 1650 6450 50  0000 R CNN
F 2 "" H 2500 5500 50  0001 C CNN
F 3 "" H 2500 5500 50  0001 C CNN
	1    2550 5500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	950  4250 2500 4250
Connection ~ 2500 4250
Wire Wire Line
	950  4700 2400 4700
Connection ~ 2400 4700
Wire Wire Line
	2400 4700 2800 4700
Wire Wire Line
	1500 4650 1700 4650
Connection ~ 1700 4650
Wire Wire Line
	1700 4650 1900 4650
Connection ~ 1900 4650
$Comp
L DriverStepperMotorTB66:driverStepperMotorTB66 U?
U 1 1 5BC3B93E
P 4250 5500
F 0 "U?" V 3400 6150 50  0000 R CNN
F 1 "driverStepperMotorTB66 axe Y" V 3300 6450 50  0000 R CNN
F 2 "" H 4200 5500 50  0001 C CNN
F 3 "" H 4200 5500 50  0001 C CNN
	1    4250 5500
	0    -1   -1   0   
$EndComp
$Comp
L DriverStepperMotorTB66:driverStepperMotorTB66 U?
U 1 1 5BC3BA86
P 5900 5500
F 0 "U?" V 5578 5472 50  0000 R CNN
F 1 "driverStepperMotorTB66 axe Z" V 5487 5472 50  0000 R CNN
F 2 "" H 5850 5500 50  0001 C CNN
F 3 "" H 5850 5500 50  0001 C CNN
	1    5900 5500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2500 4250 4200 4250
Wire Wire Line
	1900 4650 3200 4650
Wire Wire Line
	2800 4700 4100 4700
Connection ~ 4100 4700
Connection ~ 4200 4250
Connection ~ 3200 4650
Wire Wire Line
	3200 4650 3400 4650
Connection ~ 3400 4650
Wire Wire Line
	3400 4650 3600 4650
Connection ~ 3600 4650
Wire Wire Line
	3600 4650 4850 4650
Wire Wire Line
	4100 4700 5750 4700
Wire Wire Line
	4200 4250 5850 4250
Connection ~ 4850 4650
Wire Wire Line
	4850 4650 5050 4650
Connection ~ 5050 4650
Wire Wire Line
	5050 4650 5250 4650
Connection ~ 5250 4650
Wire Wire Line
	5250 4650 5700 4650
Wire Wire Line
	2000 1500 2000 4150
Wire Wire Line
	2000 4150 3500 4150
Wire Wire Line
	3300 3650 2200 3650
Wire Wire Line
	2200 3650 2200 1800
Wire Wire Line
	2100 1600 2100 4000
Wire Wire Line
	2100 4000 5150 4000
Wire Wire Line
	2300 1900 2300 3900
Wire Wire Line
	2300 3900 4950 3900
Wire Wire Line
	1800 1400 2650 1400
Wire Wire Line
	2000 1500 2650 1500
Wire Wire Line
	2100 1600 2650 1600
Wire Wire Line
	1600 1700 2650 1700
Wire Wire Line
	2200 1800 2650 1800
Wire Wire Line
	2300 1900 2650 1900
Wire Wire Line
	2800 2900 3050 2900
Wire Wire Line
	3350 800  5700 800 
Wire Wire Line
	1500 4650 1500 5500
Wire Wire Line
	1600 1700 1600 5500
Wire Wire Line
	1700 4650 1700 5500
Wire Wire Line
	1800 1400 1800 5500
Wire Wire Line
	1900 4650 1900 5500
Wire Wire Line
	2400 4700 2400 5500
Wire Wire Line
	2500 4250 2500 5500
Wire Wire Line
	4200 4250 4200 5500
Wire Wire Line
	4100 4700 4100 5500
Wire Wire Line
	3600 4650 3600 5500
Wire Wire Line
	3500 4150 3500 5500
Wire Wire Line
	3400 4650 3400 5500
Wire Wire Line
	3300 3650 3300 5500
Wire Wire Line
	3200 4650 3200 5500
Wire Wire Line
	5850 4250 5850 5500
Wire Wire Line
	5750 4700 5750 5500
Wire Wire Line
	5250 4650 5250 5500
Wire Wire Line
	5150 4000 5150 5500
Wire Wire Line
	5050 4650 5050 5500
Wire Wire Line
	4950 3900 4950 5500
Wire Wire Line
	4850 4650 4850 5500
$Comp
L Device:LED D?
U 1 1 5BC3A70D
P 4650 2450
F 0 "D?" H 4642 2666 50  0000 C CNN
F 1 "LED" H 4642 2575 50  0000 C CNN
F 2 "" H 4650 2450 50  0001 C CNN
F 3 "~" H 4650 2450 50  0001 C CNN
	1    4650 2450
	-1   0    0    -1  
$EndComp
$Comp
L Device:LED D?
U 1 1 5BC3A790
P 4650 2800
F 0 "D?" H 4642 3016 50  0000 C CNN
F 1 "LED" H 4642 2925 50  0000 C CNN
F 2 "" H 4650 2800 50  0001 C CNN
F 3 "~" H 4650 2800 50  0001 C CNN
	1    4650 2800
	-1   0    0    -1  
$EndComp
$Comp
L Device:LED D?
U 1 1 5BC3A808
P 4650 3200
F 0 "D?" H 4642 3416 50  0000 C CNN
F 1 "LED" H 4642 3325 50  0000 C CNN
F 2 "" H 4650 3200 50  0001 C CNN
F 3 "~" H 4650 3200 50  0001 C CNN
	1    4650 3200
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5BC3AA49
P 5000 2450
F 0 "R?" V 4793 2450 50  0000 C CNN
F 1 "R" V 4884 2450 50  0000 C CNN
F 2 "" V 4930 2450 50  0001 C CNN
F 3 "~" H 5000 2450 50  0001 C CNN
	1    5000 2450
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5BC3AAF3
P 5000 2800
F 0 "R?" V 4793 2800 50  0000 C CNN
F 1 "R" V 4884 2800 50  0000 C CNN
F 2 "" V 4930 2800 50  0001 C CNN
F 3 "~" H 5000 2800 50  0001 C CNN
	1    5000 2800
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5BC3AB96
P 5000 3200
F 0 "R?" V 4793 3200 50  0000 C CNN
F 1 "R" V 4884 3200 50  0000 C CNN
F 2 "" V 4930 3200 50  0001 C CNN
F 3 "~" H 5000 3200 50  0001 C CNN
	1    5000 3200
	0    1    1    0   
$EndComp
Wire Wire Line
	4800 2450 4850 2450
Wire Wire Line
	4800 2800 4850 2800
Wire Wire Line
	4800 3200 4850 3200
Wire Wire Line
	2650 2100 2400 2100
Wire Wire Line
	2400 2100 2400 3050
Wire Wire Line
	2400 3050 3750 3050
Wire Wire Line
	3750 3050 3750 2450
Wire Wire Line
	3750 2450 4500 2450
Wire Wire Line
	2650 2200 2450 2200
Wire Wire Line
	2450 2200 2450 3100
Wire Wire Line
	2450 3100 3850 3100
Wire Wire Line
	3850 3100 3850 2800
Wire Wire Line
	3850 2800 4500 2800
Wire Wire Line
	2650 2400 2500 2400
Wire Wire Line
	2500 2400 2500 3200
Wire Wire Line
	2500 3200 4500 3200
$EndSCHEMATC
