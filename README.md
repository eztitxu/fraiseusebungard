# fraiseuse Bungard

Documentation concernant la rénovation de la fraiseuse BUNGARD au fabLab La Fab'brique

But
---
Faire fonctionner une fraiseuse BUNGARD des années 80-90 qui a traîné 20 ans dans un grenier avec Arduino et GRBL

Photos
---
Montage provisoire :
![Photo du montage provisoire](./photos/IMG_20181012_180844.jpg)

Premier fraisage :
![Photo du premier fraisage](./photos/IMG_20181012_180854.jpg)

Liens utiles
---

[Letmeknow | Piloter une CNC avec arduino et GRBL](https://letmeknow.fr/blog/2016/07/11/piloter-une-cnc-avec-arduino-et-grbl/)

[LeBearCNC | Configurer et parametrer GRBL](https://lebearcnc.com/configurer-et-parametrer-grbl)

[Wiki GRBL](https://github.com/gnea/grbl/wiki)

[3DTEK | GRBL codes erreurs](https://3dtek.xyz/blogs/technical-docs/grbl-parameters-and-errors)

[Numérotation connecteur 25 broches](https://www.commentcamarche.com/contents/286-connecteur-db25)